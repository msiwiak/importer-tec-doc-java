# Tec Doc Imported / Data Quality Checker


Scripts save all logs in the file: tec-doc-importer.log after running

In file \src\main\resources\log4j.properties there is a login configuration of results

To use scripts, first run the main method from the ConfigFileTablesBuilder class to build the configuration file config\tables.json based on the config\T2ddl.sql file.

To use script edit the file config\variables.properties: oracle_connection_string and path_to_taff_folder. The remaining variables can be left

taff_folder must contain all subdirectories with producers and its data in text files, for example 002, 0311, 0161 etc. and the directory REFERENCE_DATA_*