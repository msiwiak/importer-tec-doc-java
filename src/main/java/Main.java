import scripts.DataQualityChecker;
import scripts.ImporterTecDoc;
import utils.Utils;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        List<String> tableNames = readTableNames(Utils.readVariables().get("path_to_tables_names"));
        for (String tableName : tableNames) {
            new ImporterTecDoc(tableName);
            new DataQualityChecker(tableName);
        }
    }

    private static List<String> readTableNames(String path) throws IOException {
        List<String> tableNames = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
        String tableName;
        while ((tableName = bufferedReader.readLine()) != null) {
            tableNames.add(tableName);
        }
        bufferedReader.close();
        return tableNames;
    }
}