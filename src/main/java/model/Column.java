package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Column {

    @SerializedName("columnName")
    @Expose
    private String columnName;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("length")
    @Expose
    private int length;

    public Column(String columnName, String type, int length) {
        this.columnName = columnName;
        this.type = type;
        this.length = length;
    }

    @Override
    public String toString() {
        return "model.Column{" +
                "columnName='" + columnName + '\'' +
                ", type='" + type + '\'' +
                ", length=" + length +
                '}';
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}