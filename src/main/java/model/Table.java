package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Table {

    @SerializedName("tableName")
    @Expose
    private String tableName;
    @SerializedName("length")
    @Expose
    private int length;
    @SerializedName("uniqueIndex")
    @Expose
    private List<String> uniqueIndex;
    @SerializedName("columns")
    @Expose
    private List<Column> columns;

    public Table(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "model.Table{" +
                "tableName='" + tableName + '\'' +
                ", length=" + length +
                ", uniqueIndex=" + uniqueIndex +
                ", columns=" + columns +
                '}';
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<String> getUniqueIndex() {
        return uniqueIndex;
    }

    public void setUniqueIndex(List<String> uniqueIndex) {
        this.uniqueIndex = uniqueIndex;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }
}