package scripts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import model.Column;
import model.Table;
import static utils.Utils.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ConfigFileTablesBuilder {

    private static final File ddlFile = new File(readVariables().get("path_to_T2_ddl_script"));
    private static Map<String, Table> tables = new HashMap<>();

    /**
     * The main method of the program runs the methods createTablesFromDDL and createConfigFileTables.
     *
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        createTablesFromDDL();
        createConfigFileTablesJSON(tables);
    }

    /**
     * This method saves the HashMap tables to the json format in the file config\tables.json.
     *
     * @throws IOException
     */
    private static void createConfigFileTablesJSON(Map<String, Table> tables) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(new File(readVariables().get("path_to_config_tables_json")), tables);
    }

    /**
     * This method reads the file from config\T2ddl.sql and writes the data to HashMap,
     * where the key of map is the name of the table and the value of the form of the table
     * in src\main\java\model\Table.java.
     *
     * @throws IOException
     */
    private static void createTablesFromDDL() throws IOException {
        BufferedReader originalFileReader = new BufferedReader(new FileReader(ddlFile));
        String currentLine;
        String secondLine;
        String tableName = null;
        Map<String, List<String>> uniqueIndexes = new HashMap<>();
        Set<String> types = new HashSet<>();
        while ((currentLine = originalFileReader.readLine()) != null) {
            if (currentLine.contains("create table")) {
                tableName = currentLine.split(" ")[2];
            }
            if (currentLine.contains("create unique index")) {
                String uniqueIndexLine = originalFileReader.readLine();
                String[] split = uniqueIndexLine.substring(uniqueIndexLine.indexOf("(") + 1, uniqueIndexLine.indexOf(")")).split(",");
                List<String> uniqueIndexesList = new ArrayList<>();
                for (String index : split) {
                    uniqueIndexesList.add(index.trim());
                }
                uniqueIndexes.put(uniqueIndexLine.split(" ")[1], uniqueIndexesList);
            }
            if (currentLine.equals("(")) {
                List<Column> columns = new ArrayList<>();
                Table table = new Table(tableName);
                Column column;
                int tableLength = 0;
                while (!(secondLine = originalFileReader.readLine()).equals(")")) {
                    String columnName = secondLine.split(" ")[0].trim();
                    String type = secondLine.split(" ")[1].substring(0, secondLine.split(" ")[1].indexOf("("));
                    String length;
                    types.add(type);
                    if (secondLine.split(" ")[1].indexOf(")") == -1) {
                        length = secondLine.split(" ")[1].substring(secondLine.split(" ")[1].indexOf("(") + 1);
                    } else {
                        length = secondLine.split(" ")[1].substring(secondLine.split(" ")[1].indexOf("(") + 1, secondLine.split(" ")[1].indexOf(")"));
                    }
                    tableLength = tableLength + Integer.parseInt(length);
                    column = new Column(columnName, type, Integer.parseInt(length));
                    columns.add(column);
                    table.setColumns(columns);
                    table.setLength(tableLength);
                }
                tables.put(tableName, table);
            }
        }
        for (String key : tables.keySet()) {
            tables.get(key).setUniqueIndex(uniqueIndexes.get(key));
        }
        originalFileReader.close();
    }
}