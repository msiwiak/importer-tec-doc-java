package scripts;

import model.Column;
import model.Table;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.postgresql.util.PGobject;
import java.sql.*;
import java.util.*;
import static utils.Utils.*;

public class DataQualityChecker implements Runnable {

    private static final String oracleConnectionImportedData = readVariables().get("oracle_connection_string_imported_data");
    private static final String oracleConnectionT2 = readVariables().get("oracle_connection_string_T2");
    private static final String postgreSQLConnectionCheck = readVariables().get("postgres_connection_string_jdbc");

    private static final String suffixImported = "imported";
    private static final String suffixT2 = "tecdoc";

    private static Map<String, Table> tables = readConfigFileTables();
    private String tableName;
    private Thread thread;

    private static final Logger logger = Logger.getLogger(DataQualityChecker.class);

    public DataQualityChecker(String tableName) {
        this.tableName = tableName;
        thread = new Thread(this, tableName);
        logger.info("New thread for table: " + thread);
        thread.start();
    }


    @Override
    public void run() {
        createTables(postgreSQLConnectionCheck, tableName);
        logger.info("Tables created for number: " + tableName);

        logger.info("Starting import and building hashes from imported data. Table: " + tableName);
        exportOracleToPostgreSQL(tableName, oracleConnectionImportedData, suffixImported);
        logger.info("Finished import and building hashes from imported data. Table: " + tableName);

        logger.info("Starting import and building hashes from T2 data. Table: " + tableName);
        exportOracleToPostgreSQL(tableName, oracleConnectionT2, suffixT2);
        logger.info("Finished import and building hashes from T2 data. Table: " + tableName);

        dropTables(postgreSQLConnectionCheck, tableName);
        logger.info("Tables dropped for number: " + tableName);

    }

    private static void createTables(String connectionString, String tableName) {
        Connection connection = getConnection(connectionString);
        Statement statement;
        try {
            statement = connection.createStatement();
            String sqlTablesT2 = "DROP TABLE IF EXISTS " + tableName + suffixT2 + ";CREATE  TABLE if not exists " + tableName + suffixT2 + " (id VARCHAR(32) NOT NULL PRIMARY KEY, values json NOT NULL);";
            statement.executeUpdate(sqlTablesT2);
            String sqlTablesImporter = "DROP TABLE IF EXISTS " + tableName + suffixImported + ";CREATE  TABLE if not exists " + tableName + suffixImported + " (id VARCHAR(32) NOT NULL PRIMARY KEY, values json NOT NULL);";
            statement.executeUpdate(sqlTablesImporter);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void dropTables(String connectionString, String tableName) {
        Connection connection = getConnection(connectionString);
        Statement statement;
        try {
            statement = connection.createStatement();
            String sqlTablesT2 = "DROP TABLE IF EXISTS " + tableName + suffixT2;
            statement.executeUpdate(sqlTablesT2);
            String sqlTablesImporter = "DROP TABLE IF EXISTS " + tableName + suffixImported;
            statement.executeUpdate(sqlTablesImporter);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void exportOracleToPostgreSQL(String tableName, String oracleConnectionString, String suffix) {

        int rowCount = 0;
        int fetchSize = 100000;

        Connection oracleConnection = getConnection(oracleConnectionString);
        Connection postgreSQLConnection = getConnection(postgreSQLConnectionCheck);

        Table table = tables.get(tableName);
        List<Column> columns = table.getColumns();
        List<String> uniqueIndex = table.getUniqueIndex();

        try {
            postgreSQLConnection.setAutoCommit(false);

            String sql = "Select * from " + tableName;
            PreparedStatement preparedStatement = oracleConnection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery(sql);
            resultSet.setFetchSize(fetchSize);
            PreparedStatement insertStatement = postgreSQLConnection.prepareStatement("INSERT INTO " + tableName.toLowerCase() + suffix + " (id,values) values (?,?)");

            while (resultSet.next()) {

                JSONObject row = new JSONObject();
                for (int i = 0; i < columns.size(); i++) {
                    row.put(columns.get(i).getColumnName(), resultSet.getObject(i + 1));
                }

                PGobject jsonObject = new PGobject();
                jsonObject.setType("json");
                jsonObject.setValue(row.toString());

                StringBuffer index = new StringBuffer();
                for (int i = 0; i < uniqueIndex.size(); i++) {
                    index.append(resultSet.getObject(uniqueIndex.get(i)));
                    index.append("|");
                }

                insertStatement.setObject(1, DigestUtils.md5Hex(index.toString()));
                insertStatement.setObject(2, jsonObject);
                insertStatement.addBatch();

                if (rowCount % fetchSize == 0 && rowCount != 0) {

                    insertStatement.executeBatch();
                    postgreSQLConnection.commit();
                  /*  insertStatement.clearParameters();
                    insertStatement.clearBatch();*/
                    logger.info("Inserted rows to table " + tableName + ": " + String.format("%,d", rowCount));
                }
                rowCount++;
            }

            insertStatement.executeBatch();
            postgreSQLConnection.commit();
          /*  insertStatement.clearParameters();
            insertStatement.clearBatch();*/
            logger.info("Inserted rows to table " + tableName + ": " + String.format("%,d", rowCount));

            oracleConnection.close();
            postgreSQLConnection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection(String connectionString) {
        try {
            if (connectionString.contains("postgresql")) {
                PGConnectionPoolDataSource dataSource = new PGConnectionPoolDataSource();
                dataSource.setURL(connectionString);
                return dataSource.getConnection();
            } else if (connectionString.contains("oracle")) {
                OracleDataSource oracleDataSource = new OracleDataSource();
                oracleDataSource.setURL(connectionString);
                return oracleDataSource.getConnection();
            } else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}