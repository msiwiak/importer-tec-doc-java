package scripts;

import model.Column;
import model.Table;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;
import static utils.Utils.*;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ImporterTecDoc implements Runnable {

    private static final String connectionString = readVariables().get("oracle_connection_string_imported_data");
    private static Map<String, Table> tables = readConfigFileTables();
    private static final File taffFolder = new File(readVariables().get("path_to_taff_folder"));
    private static final Logger logger = Logger.getLogger(ImporterTecDoc.class);

    private String tableName;
    private Thread thread;

    public ImporterTecDoc(String tableName) {
        this.tableName = tableName;
        thread = new Thread(this, tableName);
        logger.info("New thread for table: " + thread);
        thread.start();
    }

    @Override
    public void run() {
        try {
            logger.info("Starting import table: " + tableName);
            importTable(taffFolder, tableName);
            logger.info("Finished import table: " + tableName);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static Connection getConnection() {
        try {
            OracleDataSource oracleDataSource = new OracleDataSource();
            oracleDataSource.setURL(connectionString);
            return oracleDataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String createPreparedStatement(String tableName) {
        StringBuffer singleInsertRow = new StringBuffer("INSERT INTO " + tableName + " (");
        List<Column> columns = tables.get(tableName).getColumns();
        for (Column column : columns) {
            singleInsertRow.append(column.getColumnName() + ",");
        }
        singleInsertRow.deleteCharAt(singleInsertRow.length() - 1);
        singleInsertRow.append(") values (");

        for (Column column : columns) {
            singleInsertRow.append("?,");
        }

        singleInsertRow.deleteCharAt(singleInsertRow.length() - 1);
        singleInsertRow.append(")");
        return singleInsertRow.toString();
    }

    private static void importTable(final File taffFolder, String tableName) throws IOException, SQLException {
        Connection connection = getConnection();
        connection.setAutoCommit(false);
        PreparedStatement preparedStatement = connection.prepareStatement(createPreparedStatement(tableName));
        int rowCount = 0;
        for (final File supplierFolder : taffFolder.listFiles()) {
            if (supplierFolder.isDirectory()) {
                File[] files = supplierFolder.listFiles();
                for (File file : files) {
                    if (file.getName().split("\\.")[0].equals(tableName.substring(1))) {
                        String fileName = taffFolder + "\\" + supplierFolder.getName() + "\\" + file.getName();
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(fileName)));
                        String currentLine;
                        while ((currentLine = bufferedReader.readLine()) != null) {
                            try {
                                List<Object> tableValues = cutLine(currentLine, tableName);
                                for (int i = 0; i < tableValues.size(); i++) {
                                    preparedStatement.setObject(i + 1, tableValues.get(i));

                                }
                                preparedStatement.addBatch();
                                if (rowCount % 100000 == 0 && rowCount != 0) {
                                    preparedStatement.executeBatch();
                                    connection.commit();
                                    logger.info("Inserted rows to table " + tableName + " : " + String.format("%,d", rowCount));
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                                logger.info(e.getMessage());
                                connection.rollback();
                            }
                            rowCount++;
                        }
                        bufferedReader.close();
                    }
                }
            }
        }
        if (connection != null) {
            preparedStatement.executeBatch();
            connection.commit();
            connection.close();
        }
        logger.info("Inserted rows: " + String.format("%,d", rowCount));
    }

    private static List<Object> cutLine(String line, String tableName) {

        List<Object> strings = new ArrayList<>();
        Table table = tables.get(tableName);
        List<Column> columns = table.getColumns();

        int startNumberInString = 0;
        for (Column column : columns) {
            try {
                if (column.getType().equals("NUMBER")) {
                    String textValue = line.substring(startNumberInString, startNumberInString + column.getLength()).trim();
                    if (textValue.length() == 0) {
                        strings.add(null);
                    } else {
                        double numberValue = Double.parseDouble(textValue);
                        strings.add(String.format("%.0f", numberValue));
                    }
                } else {
                    String textValue = line.substring(startNumberInString, startNumberInString + column.getLength()).trim();
                    if (textValue.length() == 0) {
                        strings.add(null);
                    } else {
                        strings.add(textValue);
                    }
                }
            } catch (StringIndexOutOfBoundsException e) {
                strings.add(null);
            }
            startNumberInString = startNumberInString + column.getLength();
        }
        return strings;
    }
}