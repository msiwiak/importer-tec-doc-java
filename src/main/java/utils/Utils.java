package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.Table;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Utils {

    private static final String pathToVariables = "config\\variables.properties";

    /**
     * This method reads the variables from file config\variables.properties and saves them to the HashMap.
     *
     * @return HashMap of variables
     */

    public static Map<String, String> readVariables() {
        Map<String, String> variables = new HashMap<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(pathToVariables)));
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null) {
                variables.put(currentLine.split(" = ")[0], currentLine.split(" = ")[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return variables;
    }

    public static Map<String, Table> readConfigFileTables() {
        Reader reader = null;
        try {
            reader = new FileReader(readVariables().get("path_to_config_tables_json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        Map<String, Table> tables = gson.fromJson(reader, new TypeToken<Map<String, Table>>() {
        }.getType());

        return tables;
    }


}